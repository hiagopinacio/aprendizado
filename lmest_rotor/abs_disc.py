from abc import ABC, abstractmethod


class Abs_disc(ABC):
    """Class to create abstract disc mandatory properties. This is
    intended to be used for defining the standardized disc nodal
    matrices and requirements.
    """

    def __init__(self):
        pass

    @abstractmethod
    def K(self):
        pass

    @abstractmethod
    def Kst(self):
        pass

    @abstractmethod
    def C(self):
        pass

    @abstractmethod
    def G(self):
        pass

    @abstractmethod
    def M(self):
        pass
