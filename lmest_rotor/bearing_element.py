import numpy as np

from . import Abs_bearing


class BearingElement6DoF(Abs_bearing):
    """A generalistic 6 DoF bearing element.

    This class will create a bearing element with 6 DoF based on the user
    supplied stiffness and damping coefficients.
    These are determined alternatively, via purposefully built codes.

    Parameters
    ----------
    n : int
        Node in which the bearing will be inserted.
    kxx : float
        Direct stiffness in the x direction [N/m].
    cxx : float
        Direct damping in the x direction [N*s/m].
    kyy : float, optional
        Direct stiffness in the y direction [N/m].
        Defaults is 0
    cyy : float, optional
        Direct damping in the y direction [N*s/m].
        Default is 0
    kzz : float, optional
        Direct stiffness in the z direction [N/m].
        Default - kxx*0.6
    czz : float, optional
        Direct damping in the z direction [N*s/m].
        Default to cxx
    kxz : float, optional
        Cross stiffness between xz directions [N/m].
        Default is 0
    cxz : float, optional
        Cross damping between xz directions [N*s/m].
        Default is 0
    kzx : float, optional
        Cross stiffness between zx directions [N/m].
        Default is 0
    czx : float, optional
        Cross damping between zx directions [N*s/m].
        Default is 0

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> bearing = lm.BearingElement6DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
    >>> bearing.kxx
    100000.0
    """

    # @check_units
    def __init__(
        self,
        n,
        kxx,
        cxx,
        kyy=None,
        cyy=None,
        kzz=None,
        czz=None,
        kxz=None,
        cxz=None,
        kzx=None,
        czx=None,
    ):

        if kyy is None:
            kyy = 0
        if cyy is None:
            cyy = 0

        if kzz is None:
            kzz = kxx * 0.6  # NSK manufacturer sugestion for deep groove ball bearings
        if czz is None:
            czz = cxx

        if kxz is None:
            kxz = 0
        if cxz is None:
            cxz = 0
        if kzx is None:
            kzx = 0
        if czx is None:
            czx = 0

        self.n = int(n)
        self.n_l = int(n)
        self.n_r = int(n)
        self.kxx = kxx
        self.cxx = cxx
        self.kyy = kyy
        self.cyy = cyy
        self.kzz = kzz
        self.czz = czz
        self.kxz = kxz
        self.cxz = cxz
        self.kzx = kzx
        self.czx = czx

    def K(self):
        """Stiffness matrix for an instance of a bearing element.

        This method returns the stiffness matrix for an instance of a bearing element.

        Returns
        -------
        K : np.ndarray
            A 6x6 matrix of floats containing the kxx, kyy, kzz, kxz and kzx values [N/m].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> bearing = lm.BearingElement6DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
        >>> bearing.K()
        array(6x6)
        """
        kxx = self.kxx
        kxz = self.kxz
        kzx = self.kzx
        kzz = self.kzz
        kyy = self.kyy

        K = np.array(
            [
                [kxx, 0, kxz, 0, 0, 0],
                [0, kyy, 0, 0, 0, 0],
                [kzx, 0, kzz, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
            ]
        )

        return K

    def KbearingEst(self):
        """Stiffness matrix for an instance of a bearing element.

        This method returns the stiffness matrix for an instance of a bearing element.

        Returns
        -------
        K : np.ndarray
            A 6x6 matrix of floats containing the kxx, kyy and kzz values [N/m].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> bearing = lm.BearingElement6DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
        >>> bearing.KbearingEst()
        array(6x6)
        """
        kxx = self.kxx
        kxz = self.kxz
        kzx = self.kzx
        kzz = self.kzz
        kyy = self.kyy

        KbearingEst = np.array(
            [
                [kxx, 0, 0, 0, 0, 0],
                [0, kyy, 0, 0, 0, 0],
                [0, 0, kzz, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
            ]
        )

        return KbearingEst

    def C(self):
        """Damping matrix for an instance of a bearing element.

        This method returns the damping matrix for an instance of a bearing element.

        Returns
        -------
        C: np.ndarray
            A 6x6 matrix of floats containing the cxx, cyy, czz, cxz and czx values [N*s/m].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> bearing = lm.BearingElement6DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
        >>> bearing.C()
        array(6x6)
        """
        cxx = self.cxx
        cxz = self.cxz
        czx = self.czx
        czz = self.czz
        cyy = self.cyy

        C = np.array(
            [
                [cxx, 0, cxz, 0, 0, 0],
                [0, cyy, 0, 0, 0, 0],
                [czx, 0, czz, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
            ]
        )

        return C


class BearingElement4DoF(Abs_bearing):
    """A bearing element.

    This class will create a bearing element with 4 DoF based on the user
    supplied stiffness and damping coefficients.
    These are determined alternatively, via purposefully built codes.

    Parameters
    ----------
    n : int
        Node in which the bearing will be inserted.
    kxx : float
        Direct stiffness in the x direction [N/m].
    cxx : float
        Direct damping in the x direction [N*s/m].
    kzz : float, optional
        Direct stiffness in the z direction [N/m].
        Default - kxx*0.6
    czz : float, optional
        Direct damping in the z direction [N*s/m].
        Default to cxx
    kxz : float, optional
        Cross stiffness between xz directions [N/m].
        Default is 0
    cxz : float, optional
        Cross damping between xz directions [N*s/m].
        Default is 0
    kzx : float, optional
        Cross stiffness between zx directions [N/m].
        Default is 0
    czx : float, optional
        Cross damping between zx directions [N*s/m].
        Default is 0


    Examples
    --------
    >>> import lmest_rotor as lm
    >>> bearing = lm.BearingElement4DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
    >>> bearing.kzz
    1000000.0
    """

    # @check_units
    def __init__(
        self,
        n,
        kxx,
        cxx,
        kzz=None,
        czz=None,
        kxz=None,
        cxz=None,
        kzx=None,
        czx=None,
    ):

        if kzz is None:
            kzz = kxx * 0.6  # NSK manufacturer sugestion for deep groove ball bearings
        if czz is None:
            czz = cxx

        if kxz is None:
            kxz = 0
        if cxz is None:
            cxz = 0
        if kzx is None:
            kzx = 0
        if czx is None:
            czx = 0

        self.n = int(n)
        self.n_l = int(n)
        self.n_r = int(n)
        self.kxx = kxx
        self.cxx = cxx
        self.kzz = kzz
        self.czz = czz
        self.kxz = kxz
        self.cxz = cxz
        self.kzx = kzx
        self.czx = czx

    def K(self):
        """Stiffness matrix for an instance of a bearing element.

        This method returns the stiffness matrix for an instance of a bearing element.

        Returns
        -------
        K : np.ndarray
            A 4x4 matrix of floats containing the kxx, kzz, kxz and kzx values [N/m].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> bearing = lm.BearingElement4DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
        >>> bearing.K()
        array(4x4)
        """
        kxx = self.kxx
        kxz = self.kxz
        kzx = self.kzx
        kzz = self.kzz

        K = np.array(
            [
                [kxx, kxz, 0, 0],
                [kzx, kzz, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )

        return K

    def KbearingEst(self):
        """Stiffness matrix for an instance of a bearing element.

        This method returns the stiffness matrix for an instance of a bearing element.

        Returns
        -------
        K : np.ndarray
            A 4x4 matrix of floats containing the kxx, kyy and kzz values [N/m].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> bearing = lm.BearingElement4DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
        >>> bearing.KbearingEst()
        array(4x4)
        """
        kxx = self.kxx
        kxz = self.kxz
        kzx = self.kzx
        kzz = self.kzz

        KbearingEst = np.array(
            [
                [kxx, 0, 0, 0],
                [0, kzz, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )

        return KbearingEst

    def C(self):
        """Damping matrix for an instance of a bearing element.

        This method returns the damping matrix for an instance of a bearing element.

        Returns
        -------
        C: np.ndarray
            A 4x4 matrix of floats containing the cxx, cyy, czz, cxz and czx values [N*s/m].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> bearing = lm.BearingElement4DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)
        >>> bearing.C()
        array(4x4)
        """
        cxx = self.cxx
        cxz = self.cxz
        czx = self.czx
        czz = self.czz

        C = np.array(
            [
                [cxx, cxz, 0, 0],
                [czx, czz, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )

        return C
