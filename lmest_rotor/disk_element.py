import numpy as np

from . import Abs_disc


class DiskElement6DoF(Abs_disc):
    """A disk element for 6 DoFs.

    This class will create a disk element with 6 DoF from input data of inertia and
    mass.

    Parameters
    ----------
    n: int
        Node in which the disk will be inserted.
    m : float
        Mass of the disk element [kg].
    Id : float
        Moment of inertia Idx=Idz - 1/12.Md.(3R**2 + 3r**2 + d**2)[kg.(m**2)].
    Ip : float
        Moment of inertia Idy - 1/2.Md.(R**2 + r**2)[kg.(m**2)].

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> from lmest_rotor import Material
    >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
    >>> disk.Ip
    0.002
    """

    # @check_units
    def __init__(self, n, m, Id, Ip):
        self.n = int(n)
        self.n_l = n
        self.n_r = n

        self.m = float(m)
        self.Id = float(Id)
        self.Ip = float(Ip)

    def M(self):
        """Mass matrix for an instance of a 6 DoF disk element.

        This method will return the mass matrix for an instance of a disk
        element with 6DoFs.

        Returns
        -------
        M : np.ndarray
            Mass matrix for the 6DoFs disk element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.M()
        array(6x6)
        """
        m = self.m
        Id = self.Id
        Ip = self.Ip
        # fmt: off
        M = np.array([
            [m,  0,  0,  0,  0,  0],
            [0,  m,  0,  0,  0,  0],
            [0,  0,  m,  0,  0,  0],
            [0,  0,  0, Id,  0,  0],
            [0,  0,  0,  0, Ip,  0],
            [0,  0,  0,  0,  0, Id],
        ])
        # fmt: on
        return M

    def K(self):
        """Stiffness matrix for an instance of a 6 DoF disk element.

        Returns
        -------
        K : np.ndarray
            A matrix of floats containing the values of the stiffness matrix.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.C()
        array(6x6)
        """

        K = np.zeros((6, 6))

        return K

    def Kst(self):
        """Dynamic stiffness matrix for an instance of a 6 DoF disk element.

        This method will return the stiffness matrix for an instance of a disk
        element with 6DoFs.

        Returns
        -------
        Kst : np.ndarray
            Dynamic stiffness matrix for the 6 DoF disk element. This is
            directly dependent on the rotation speed Omega. It needs to be
            multiplied by the adequate Omega value when used in time depen-
            dent analyses.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.Kst()
        array(6x6)
        """
        Ip = self.Ip
        # fmt: off
        Kst = np.array([
            [0, 0, 0,  0, 0, 0],
            [0, 0, 0,  0, 0, 0],
            [0, 0, 0,  0, 0, 0],
            [0, 0, 0,  0, 0, 0],
            [0, 0, 0, Ip, 0, 0],
            [0, 0, 0,  0, 0, 0],
        ])
        # fmt: on
        return Kst

    def C(self):
        """Damping matrix for an instance of a 6 DoF disk element.

        Returns
        -------
        C : np.ndarray
            A matrix of floats containing the values of the damping matrix.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.C()
        array(6x6)
        """
        C = np.zeros((6, 6))

        return C

    def G(self):
        """Gyroscopic matrix for an instance of a 6 DoF disk element.

        This method will return the gyroscopic matrix for an instance of a disk
        element.

        Returns
        -------
        G : np.ndarray
            Gyroscopic matrix for the disk element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.G()
        array(6x6)
        """
        Ip = self.Ip
        # fmt: off
        G = np.array([
            [0, 0, 0,  0,  0,  0],
            [0, 0, 0,  0,  0,  0],
            [0, 0, 0,  0,  0,  0],
            [0, 0, 0,  0,  0,-Ip],
            [0, 0, 0,  0,  0,  0],
            [0, 0, 0, Ip,  0,  0],
        ])
        # fmt: on
        return G


class DiskElement4DoF(Abs_disc):
    """A disk element for 4 DoFs.

    This class will create a disk element with 4 DoF from input data of inertia and
    mass.

    Parameters
    ----------
    n: int
        Node in which the disk will be inserted.
    m : float
        Mass of the disk element [kg].
    Id : float
        Moment of inertia Idx=Idz - 1/12.Md.(3R**2 + 3r**2 + d**2)[kg.(m**2)].
    Ip : float
        Moment of inertia Idy - 1/2.Md.(R**2 + r**2)[kg.(m**2)].

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> from lmest_rotor import Material
    >>> disk = lm.DiskElement4DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
    >>> disk.Ip
    0.002
    """

    # @check_units
    def __init__(self, n, m, Id, Ip):
        self.n = int(n)
        self.n_l = n
        self.n_r = n

        self.m = float(m)
        self.Id = float(Id)
        self.Ip = float(Ip)

    def M(self):
        """Mass matrix for an instance of a disk element.

        This method will return the mass matrix for an instance of a disk element.

        Returns
        -------
        M : np.ndarray
            A matrix of floats containing the values of the mass matrix.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement4DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.M()
        array(4x4)
        """
        m = self.m
        Id = self.Id
        # fmt: off
        M = np.array([[m, 0,  0,  0],
                        [0, m,  0,  0],
                        [0, 0, Id,  0],
                        [0, 0,  0, Id]])
        # fmt: on
        return M

    def K(self):
        """Stiffness matrix for an instance of a disk element.

        This method will return the stiffness matrix for an instance of a disk
        element.

        Returns
        -------
        K : np.ndarray
            A matrix of floats containing the values of the stiffness matrix.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement4DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.K()
        array(4x4)
        """
        K = np.zeros((4, 4))

        return K

    def Kst(self):
        """Dynamic stiffness matrix for an instance of a disk element.

        This method will return the stiffness matrix for an instance of a disk
        element with 4DoFs.

        Returns
        -------
        Kst : np.ndarray
            Dynamic stiffness matrix for the 4 DoF disk element. This is
            directly dependent on the rotation speed Omega.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.Kst()
        array(4x4)
        """

        Kst = np.zeros((4, 4))

        return Kst

    def C(self):
        """Damping matrix for an instance of a disk element.

        Returns
        -------
        C : np.ndarray
            A matrix of floats containing the values of the damping matrix.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.C()
        array(4x4)
        """
        C = np.zeros((4, 4))

        return C

    def G(self):
        """Gyroscopic matrix for an instance of a disk element.

        This method will return the gyroscopic matrix for an instance of a disk
        element.

        Returns
        -------
        G: np.ndarray
            Gyroscopic matrix for the disk element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> disk = lm.DiskElement6DoF(n=8, m=(658.05e-3), Id=0.0010, Ip=0.002)
        >>> disk.G()
        array(4x4)
        """
        Ip = self.Ip
        # fmt: off
        G = np.array([[0, 0,   0,  0],
                      [0, 0,   0,  0],
                      [0, 0,   0, -Ip],
                      [0, 0, Ip,  0]])
        # fmt: on
        return G
