"""Time integration module.

This module defines the time integration routines to evaluate time responses from
the rotors. These simulate the transient evolution of the dynamic behavior of the
rotors defined by the user, and return the time signals for rotor displacement.

"""

import time

import numpy as np


class NewmarkMethod(object):
    """Newmark and Newton-Raphson solution.

    The solution of the equations was obtained by using the trapezoidal rule integration
    scheme (Newmark), which was coupled with the Newton Raphson iterative method for
    nonlinear equations. The modal domain is considered.

    Parameters
    ----------
    DOF : int
        Auxiliary integer for 4 or 6 degrees of freedom.
    ndof : int
        Number of degrees of freedom.
    dt : float
        Time step [s].
    T : array
        Time vector [s].
    Omega : array
        Speed Vector [rad/s].
    AccelV : array
        Acceleration vector [rad/(s**2)].
    TorqueV : array
        Torque vector [N.m].
    Funbmodal : array
        Unbalance forces vector (modal).
    MODALmatrix : array
        Rotor modal matrix.
    MMmodal : array
        Modal mass matrix.
    KKmodal : array
        Modal stiffness matrix.
    KKstmodal : array
        Modal dynamic stiffness matrix.
    KKfmodal : array
        Modal stiffness matrix due to the axial force.
    KKtmodal : array
        Modal stiffness matrix due to the torque.
    CCgyrosmodal : array
        Modal gyroscopic matrix.
    CCtotalmodal : array
        Modal damping matrix.
    Wmodal : array
        Modal weight vector.
    Faxialmodal : array
        Axial forces vector (modal).
    FFtorquemodal : array
        Torque vector (modal).

    Attributes
        ----------
    yy : array
        Displacement vector of all degrees of freedom.
    """

    def __init__(
        self,
        DOF,
        ndof,
        dt,
        T,
        Omega,
        AccelV,
        TorqueV,
        Funbmodal,
        MODALmatrix,
        MMmodal,
        KKmodal,
        KKstmodal,
        KKfmodal,
        KKtmodal,
        CCgyrosmodal,
        CCtotalmodal,
        Wmodal,
        Faxialmodal,
        FFtorquemodal,
        print_progress,
    ):
        self.DOF = DOF
        self.print_progress = print_progress
        self.T = T
        n = abs((int)((T[0] - T[-1]) / dt))
        INTerror = 1e-6
        ymodal = np.zeros(12)
        yptmodal = ymodal
        y2ptmodal = ymodal

        yaux = np.zeros((len(ymodal), len(T)))

        self.iteration = 0
        t1 = time.time()

        for jj in range(1, (n + 1)):
            self.iteration += 1
            if self.iteration % 1000 == 0 and self.print_progress:
                print(f"iteration: {self.iteration} \n time: {T[jj]}")

            y2ptfuturemodal = y2ptmodal
            yptaux = y2ptmodal + y2ptfuturemodal
            yptfuturemodal = yptmodal + 0.5 * dt * yptaux
            yfuturemodal = ymodal + dt * yptmodal + 0.25 * dt ** 2 * yptaux

            CCaux = CCtotalmodal + CCgyrosmodal * Omega[jj]
            Kaux = KKmodal + KKfmodal + TorqueV[jj] * KKtmodal

            KKaux = Kaux + KKstmodal * AccelV[jj]

            Rtdt = (
                MMmodal.dot(y2ptfuturemodal)
                + CCaux.dot(yptfuturemodal)
                + KKaux.dot(yfuturemodal)
                - Funbmodal[:, jj]
                - Wmodal
                - Faxialmodal
                - FFtorquemodal[:, jj]
            )

            Nerro = 0
            # while np.sqrt(np.sum(np.square(Rtdt))) > INTerror:
            while np.linalg.norm(Rtdt) > INTerror:
                Nerro += 1
                if Nerro >= 1000:
                    print(f"iteration of while: \n time: {Nerro}")

                Tdt = MMmodal + 0.5 * dt * CCaux + 0.25 * dt ** 2 * KKaux

                # Variations / Du, Dupt and Du2pt

                Dy2ptfuturemodal = -(np.linalg.solve(Tdt, Rtdt))
                Dy2ptaux = dt * Dy2ptfuturemodal
                Dyptfuturemodal = 0.5 * Dy2ptaux
                Dyfuturemodal = 0.25 * dt * Dy2ptaux

                y2ptfuturemodal += Dy2ptfuturemodal
                yptfuturemodal += Dyptfuturemodal
                yfuturemodal += Dyfuturemodal

                Rtdt = (
                    MMmodal.dot(y2ptfuturemodal)
                    + CCaux.dot(yptfuturemodal)
                    + KKaux.dot(yfuturemodal)
                    - Funbmodal[:, jj]
                    - Wmodal
                    - Faxialmodal
                    - FFtorquemodal[:, jj]
                )

            ymodal = yfuturemodal
            yptmodal = yptfuturemodal
            y2ptmodal = y2ptfuturemodal

            yaux[:, jj] = ymodal
            # self.yy[:, jj] = MODALmatrix.dot(ymodal)
            t2 = time.time()

            self.tt = t2 - t1
        self.yy = MODALmatrix.dot(yaux)
