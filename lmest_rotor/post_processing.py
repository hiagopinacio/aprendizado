import numpy as np
import plotly.graph_objects as go
import scipy as sp
from scipy import signal

from .rotor_assembly import Rotor


class PostProcessing(object):
    """Plotting module.

    This module returns graphs for each type of analyses in rotor_assembly.py.

    Parameters
    ----------
    rotor : object, optional
        Rotor object.
    probe : list, optional
        List with probes according to their position [Node].
    output : object, optional
        Time response of the system.

    Returns
    -------
    fig : Plotly graph_objects.Figure()
        The figure object with the plot.
    """

    def __init__(self, rotor=None, probe=None, output=None):
        self.rotor = rotor
        self.probe = probe
        self.output = output

    def plot_rotor(self):
        """Plot a rotor object.

        This function will take a rotor object and plot its elements representation by
        using Plotly.

        Returns
        -------
        fig : plotly.graph_objects.Figure
            The figure object with the rotor representation.

        Example
        -------
        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> figura = PostProcessing(rotor, [probe1, probe2]).plot_rotor().show()
        """

        self.shaft_elements = self.rotor.shaft_elements
        self.disk_elements = self.rotor.disk_elements
        self.bearing_elements = self.rotor.bearing_elements

        mat_shaft = np.zeros((len(self.shaft_elements), 4))
        mat_disk = np.zeros((len(self.disk_elements), 5))

        if self.rotor.DOF == 6:
            mat_bearing = np.zeros((len(self.bearing_elements), 8))
        else:
            mat_bearing = np.zeros((len(self.bearing_elements), 6))
        xa = np.zeros((len(self.disk_elements)))
        xb = np.zeros((len(self.bearing_elements)))

        L_elementos = np.zeros((len(self.shaft_elements) + 1))
        L_aux = np.zeros((len(self.shaft_elements)))

        for i in range(1, len(self.shaft_elements) + 1):
            L_elementos[i] = L_elementos[i - 1] + self.shaft_elements[i - 1].L
            L_aux[i - 1] = L_elementos[i - 1] + (self.shaft_elements[i - 1].L) / 2

        for ii in range(len(self.shaft_elements)):
            mat_shaft[ii, :] = [
                ii,
                self.shaft_elements[ii].L,
                self.shaft_elements[ii].id,
                self.shaft_elements[ii].od,
            ]

        figure = go.Figure()

        for ii in range(len(self.shaft_elements)):
            if ii == 0:
                condition = True
            else:
                condition = False

            figure.add_trace(
                go.Scatter(
                    x=[
                        L_elementos[ii],
                        L_elementos[ii + 1],
                        L_elementos[ii + 1],
                        L_elementos[ii],
                        L_elementos[ii],
                    ],
                    y=[
                        self.shaft_elements[ii].od / 2,
                        self.shaft_elements[ii].od / 2,
                        -self.shaft_elements[ii].od / 2,
                        -self.shaft_elements[ii].od / 2,
                        self.shaft_elements[ii].od / 2,
                    ],
                    mode="lines",
                    line=dict(color="rgb(54,54,54)", width=2),
                    legendgroup="Shaft",
                    showlegend=False,
                )
            )

            figure.add_trace(
                go.Scatter(
                    x=[L_aux[ii]],
                    y=[0],
                    name="Elements number",
                    mode="markers+text",
                    text=f"{int(mat_shaft[ii, 0])}",
                    textposition="middle center",
                    textfont=dict(
                        family="Times New Roman", size=10, color="rgb(0,0,0)"
                    ),
                    marker=dict(
                        symbol="circle-dot",
                        size=15,
                        color="rgb(173,216,230)",
                        opacity=0.5,
                    ),
                    marker_line=dict(width=1, color="rgb(70,130,180)"),
                    hovertext=(
                        f"Element Number: {int(mat_shaft[ii, 0])}<br>"
                        + f"Element Length: {round(mat_shaft[ii, 1], 3)}<br>"
                        + f"Inner Diameter: {mat_shaft[ii, 2]}<br>"
                        + f"Outer Diameter: {mat_shaft[ii, 3]}",
                    ),
                    hoverinfo="text",
                    legendgroup="Nodes",
                    showlegend=condition,
                )
            )
        figure.update_traces(
            fill="toself",
            fillcolor="rgb(169,169,169)",
            opacity=1,
        )

        if not self.disk_elements:
            pass
        else:
            scale_disk_h = 10
            scale_disk_w = 0.002

            for ii in range(len(self.disk_elements)):
                if ii == 0:
                    condition = True
                else:
                    condition = False

                mat_disk[ii, :] = [
                    ii,
                    self.disk_elements[ii].n,
                    self.disk_elements[ii].m,
                    self.disk_elements[ii].Id,
                    self.disk_elements[ii].Ip,
                ]

                xa[ii] = L_elementos[int(mat_disk[ii, 1])]

                hover_text = (
                    f"Disk node: {int(mat_disk[ii, 1])}<br>"
                    + f"Disk Mass: {round(mat_disk[ii, 2], 3)}<br>"
                    + f"Diametral Inertia: {round(mat_disk[ii, 3],4)}<br>"
                    + f"Polar Inertia: {round(mat_disk[ii, 4],4)}"
                )

                figure.add_trace(
                    go.Scatter(
                        name="Disk",
                        x=[
                            xa[ii],
                            xa[ii] + scale_disk_w,
                            xa[ii] - scale_disk_w,
                            xa[ii],
                        ],
                        y=[
                            0 + max(mat_shaft[:, 3] / 2),
                            +scale_disk_h * max(mat_shaft[:, 3] / 2),
                            +scale_disk_h * max(mat_shaft[:, 3] / 2),
                            0 + max(mat_shaft[:, 3] / 2),
                        ],
                        text=hover_text,
                        fill="toself",
                        fillcolor="rgb(128,0,0)",
                        mode="lines",
                        line=dict(color="rgb(128,0,0)", width=2),
                        hoveron="points+fills",
                        hoverinfo="text",
                        hovertemplate=hover_text,
                        legendgroup="Disk",
                        showlegend=condition,
                    )
                )

                figure.add_trace(
                    go.Scatter(
                        name="Disk",
                        x=[
                            xa[ii],
                            xa[ii] + scale_disk_w,
                            xa[ii] - scale_disk_w,
                            xa[ii],
                        ],
                        y=[
                            0 - max(mat_shaft[:, 3] / 2),
                            -scale_disk_h * max(mat_shaft[:, 3] / 2),
                            -scale_disk_h * max(mat_shaft[:, 3] / 2),
                            0 - max(mat_shaft[:, 3] / 2),
                        ],
                        text=hover_text,
                        fill="toself",
                        fillcolor="rgb(128,0,0)",
                        mode="lines",
                        line=dict(color="rgb(128,0,0)", width=2),
                        hoveron="points+fills",
                        hoverinfo="text",
                        hovertemplate=hover_text,
                        legendgroup="Disk",
                        showlegend=False,
                    )
                )

        if not self.bearing_elements:
            pass
        else:
            scale_bearing_h = 5
            scale_bearing_w = 0.002

            for ii in range(len(self.bearing_elements)):
                if ii == 0:
                    condition = True
                else:
                    condition = False
                if self.rotor.DOF == 6:
                    mat_bearing[ii, :] = [
                        ii,
                        self.bearing_elements[ii].n,
                        self.bearing_elements[ii].kxx,
                        self.bearing_elements[ii].kzz,
                        self.bearing_elements[ii].kyy,
                        self.bearing_elements[ii].cxx,
                        self.bearing_elements[ii].czz,
                        self.bearing_elements[ii].cyy,
                    ]

                    xb[ii] = L_elementos[int(mat_bearing[ii, 1])]

                    hover_text = (
                        f"Bearing node: {int(mat_bearing[ii, 1])}<br>"
                        + f"kxx: {mat_bearing[ii, 2]:.2e}  "
                        + f"cxx: {mat_bearing[ii, 5]:.2e}<br>"
                        + f"kzz: {mat_bearing[ii, 3]:.2e}  "
                        + f"czz: {mat_bearing[ii, 6]:.2e}<br>"
                        + f"kyy: {mat_bearing[ii, 4]:.2e}  "
                        + f"cyy: {mat_bearing[ii, 7]:.2e}"
                    )

                else:
                    mat_bearing[ii, :] = [
                        ii,
                        self.bearing_elements[ii].n,
                        self.bearing_elements[ii].kxx,
                        self.bearing_elements[ii].kzz,
                        self.bearing_elements[ii].cxx,
                        self.bearing_elements[ii].czz,
                    ]

                    xb[ii] = L_elementos[int(mat_bearing[ii, 1])]

                    hover_text = (
                        f"Bearing node: {int(mat_bearing[ii, 1])}<br>"
                        + f"kxx: {mat_bearing[ii, 2]:.2e}  "
                        + f"cxx: {mat_bearing[ii, 3]:.2e}<br>"
                        + f"kzz: {mat_bearing[ii, 4]:.2e}  "
                        + f"czz: {mat_bearing[ii, 5]:.2e}<br>"
                    )

                figure.add_trace(
                    go.Scatter(
                        x=[
                            xb[ii],
                            xb[ii] + scale_bearing_w,
                            xb[ii] - scale_bearing_w,
                            xb[ii],
                        ],
                        y=[
                            0 + max(mat_shaft[:, 3] / 2),
                            +scale_bearing_h * max(mat_shaft[:, 3] / 2),
                            +scale_bearing_h * max(mat_shaft[:, 3] / 2),
                            0 + max(mat_shaft[:, 3] / 2),
                        ],
                        text=hover_text,
                        fill="toself",
                        fillcolor="rgb(0,0,139)",
                        mode="lines",
                        line=dict(color="rgb(0,0,139)", width=2),
                        name="Bearing",
                        legendgroup="Bearing",
                        hoveron="points+fills",
                        hoverinfo="text",
                        hovertemplate=hover_text,
                        showlegend=condition,
                    )
                )

                figure.add_trace(
                    go.Scatter(
                        x=[
                            xb[ii],
                            xb[ii] + scale_bearing_w,
                            xb[ii] - scale_bearing_w,
                            xb[ii],
                        ],
                        y=[
                            0 - max(mat_shaft[:, 3] / 2),
                            -scale_bearing_h * max(mat_shaft[:, 3] / 2),
                            -scale_bearing_h * max(mat_shaft[:, 3] / 2),
                            0 - max(mat_shaft[:, 3] / 2),
                        ],
                        text=hover_text,
                        fill="toself",
                        fillcolor="rgb(0,0,139)",
                        mode="lines",
                        line=dict(color="rgb(0,0,139)", width=2),
                        name="Bearing",
                        legendgroup="Bearing",
                        hoveron="points+fills",
                        hoverinfo="text",
                        hovertemplate=hover_text,
                        showlegend=False,
                    )
                )

        if self.probe is None:
            pass
        else:
            scale_probes_h = 3
            scale_probes_w = 0.002

            for ii in range(len(self.probe)):
                if ii == 0:
                    condition = True
                else:
                    condition = False

                xp = L_elementos[int(self.probe[ii])]

                hover_text = f"Probe node: {self.probe[ii]}<br>"

                figure.add_trace(
                    go.Scatter(
                        x=[
                            xp,
                            xp + scale_probes_w,
                            xp - scale_probes_w,
                            xp,
                        ],
                        y=[
                            0 + max(mat_shaft[:, 3] / 2),
                            +scale_probes_h * max(mat_shaft[:, 3] / 2),
                            +scale_probes_h * max(mat_shaft[:, 3] / 2),
                            0 + max(mat_shaft[:, 3] / 2),
                        ],
                        text=hover_text,
                        fill="toself",
                        fillcolor="rgb(0, 102, 0)",
                        mode="lines",
                        line=dict(color="rgb(0, 102, 0)", width=2),
                        name="Probes",
                        legendgroup="Probes",
                        hoveron="points+fills",
                        hoverinfo="text",
                        hovertemplate=hover_text,
                        showlegend=condition,
                    )
                )

                figure.add_trace(
                    go.Scatter(
                        x=[
                            xp,
                            xp + scale_probes_w,
                            xp - scale_probes_w,
                            xp,
                        ],
                        y=[
                            0 - max(mat_shaft[:, 3] / 2),
                            -scale_probes_h * max(mat_shaft[:, 3] / 2),
                            -scale_probes_h * max(mat_shaft[:, 3] / 2),
                            0 - max(mat_shaft[:, 3] / 2),
                        ],
                        text=hover_text,
                        fill="toself",
                        fillcolor="rgb(0, 102, 0)",
                        mode="lines",
                        line=dict(color="rgb(0, 102, 0)", width=2),
                        name="Probes",
                        legendgroup="Probes",
                        hoveron="points+fills",
                        hoverinfo="text",
                        hovertemplate=hover_text,
                        showlegend=False,
                    )
                )

        scale = abs(L_elementos[1] - L_elementos[-1])
        figure.update_layout(
            legend=dict(
                orientation="h",
                yanchor="top",
                y=0.98,
                xanchor="center",
                x=0.5,
                font=dict(family="Times New Roman", size=16),
            )
        )
        # figure.update_traces(showlegend=True, selector=dict(type="scatter"))
        figure.update_layout(
            xaxis_range=[L_elementos[0] - scale / 10, L_elementos[-1] + scale / 10],
            yaxis_range=[-scale / 3, scale / 3],
            template="plotly_white",
        )
        figure.update_yaxes(
            title=dict(
                text=f"Shaft diameter (m)", font=dict(family="Times New Roman", size=16)
            ),
            tickfont=dict(family="Times New Roman", size=16),
        )
        figure.update_xaxes(
            title=dict(
                text=f"Shaft length (m)", font=dict(family="Times New Roman", size=16)
            ),
            tickfont=dict(family="Times New Roman", size=16),
        )
        return figure

    def plot_time_response(self):
        """Plot time response (magnitude) using Plotly.

        This method plots the time response magnitude given an output and
        an input using Plotly.

        Returns
        -------
        fig : Plotly graph_objects.Figure()
            The figure object with the plot.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> x = rotor.run(0, 0.01, 10, [1e-4], [0], 1200)
        >>> results = PostProcessing(rotor, [2], x).plot_time_response()[0].show()
        """

        results = self.output.yy
        temp = self.output.T
        self.DOF = self.output.DOF
        if self.DOF == 6:
            aux_dof = 2
        else:
            aux_dof = 1

        fig = []

        for i in list(range(len(self.probe))):
            figure = go.Figure()

            dofx = self.probe[i] * self.DOF  # Number of degrees os freedom
            dofz = self.probe[i] * self.DOF + aux_dof

            signal_x = results[dofx, :]
            signal_z = results[dofz, :]

            figure.add_trace(
                go.Scatter(x=temp, y=signal_x, mode="lines", name="X-axis")
            )
            figure.add_trace(
                go.Scatter(x=temp, y=signal_z, mode="lines", name="Z-axis")
            )
            figure.update_layout(
                title=dict(
                    text=f"Probe {i + 1} - GDL {dofx} e {dofz}",
                    font=dict(family="Times New Roman", size=16),
                )
            )
            figure.update_yaxes(
                title=dict(
                    text=f"Amplitude (m)", font=dict(family="Times New Roman", size=16)
                ),
                tickfont=dict(family="Times New Roman", size=16),
            )
            figure.update_xaxes(
                title=dict(
                    text=f"Time (s)", font=dict(family="Times New Roman", size=16)
                ),
                tickfont=dict(family="Times New Roman", size=16),
            )

            figure.update_layout(
                legend=dict(font=dict(family="Times New Roman", size=16))
            )
            fig.append(figure)

        return fig

    def plot_dfft(self, range_freq=None, **kwargs):
        """Plot response in frequency domain (dFFT - discrete Fourier Transform) by using Plotly.

        Parameters
        ----------
        range_freq : list, optional
            Units for the x axis.
            Default is "Hz"
        kwargs : optional
            Additional key word arguments can be passed to change the plot layout only
            (e.g. width=1000, height=800, yaxis_type="log", ...).
            *See Plotly Python Figure Reference for more information.

        Returns
        -------
        fig : Plotly graph_objects.Figure()
            The figure object with the plot.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> x = rotor.run(0, 0.01, 10, [1e-4], [0], 1200)
        >>> results = PostProcessing(rotor,[2], x).plot_dfft([0, 40], yaxis_type="log")[0].show()
        """
        results = self.output.yy
        temp = self.output.T
        self.DOF = self.output.DOF
        if self.DOF == 6:
            aux_dof = 2
        else:
            aux_dof = 1
        dt = temp[1] - temp[0]
        fig = []

        for i in list(range(len(self.probe))):
            figure = go.Figure()

            dofx = self.probe[i] * self.DOF  # Number of degrees os freedom
            dofz = self.probe[i] * self.DOF + aux_dof

            rows, cols = results.shape

            signal_x = results[dofx, :]
            signal_z = results[dofz, :]

            amp_x, freq = self._dfft(signal_x[int(2 * cols / 3) :], dt)
            amp_z = self._dfft(signal_z[int(2 * cols / 3) :], dt)[0]

            if range_freq is not None:
                amp_x = amp_x[(freq >= range_freq[0]) & (freq <= range_freq[1])]
                amp_z = amp_z[(freq >= range_freq[0]) & (freq <= range_freq[1])]

                freq = freq[(freq >= range_freq[0]) & (freq <= range_freq[1])]

            # if freq_cut is None:
            #     pass
            # else:
            #     aux = freq_cut - freq
            #     where = np.argmin(np.abs(aux))
            #     freq = freq[: (int(np.round(where)) + 1)]
            #     amp_x = amp_x[: (int(np.round(where)) + 1)]
            #     amp_z = amp_z[: (int(np.round(where)) + 1)]

            figure.add_trace(
                go.Scatter(
                    x=freq,
                    y=amp_x,
                    mode="lines",
                    name="X-axis",
                )
            )
            figure.add_trace(
                go.Scatter(
                    x=freq,
                    y=amp_z,
                    mode="lines",
                    name="Z-axis",
                )
            )
            figure.update_layout(
                title=dict(
                    text=f"Probe {i + 1} - GDL {dofx} e {dofz}",
                    font=dict(family="Times New Roman", size=16),
                )
            )
            figure.update_yaxes(
                title=dict(
                    text=f"Amplitude (m)", font=dict(family="Times New Roman", size=16)
                ),
                tickfont=dict(family="Times New Roman", size=16),
            )
            figure.update_xaxes(
                title=dict(
                    text=f"Frequency (Hz)", font=dict(family="Times New Roman", size=16)
                ),
                tickfont=dict(family="Times New Roman", size=16),
            )
            figure.update_layout(**kwargs)
            figure.update_layout(
                legend=dict(font=dict(family="Times New Roman", size=16))
            )
            figure.update_layout(xaxis_range=[range_freq[0], range_freq[1]])

            fig.append(figure)

        return fig

    def _dfft(self, x, dt):
        """Calculate dFFT - discrete Fourier Transform.

        Parameters
        ----------
        x : np.array
            Magnitude of the response in time domain.
            Default is "m".
        dt : int
            Time step.
            Default is "s".

        Returns
        -------
        x_amp : np.array
            Amplitude of the response in frequency domain.
            Default is "m".
        freq : np.array
            Frequency range.
            Default is "Hz".
        """

        b = np.floor(len(x) / 2)
        c = len(x)
        df = 1 / (c * dt)

        x_amp = sp.fft(x)[: int(b)]
        x_amp = x_amp * 2 / c
        x_phase = np.angle(x_amp)
        x_amp = np.abs(x_amp)

        freq = np.arange(0, df * b, df)
        freq = freq[: int(b)]  # Frequency vector

        return x_amp, freq

    def plot_campbell(self, out_campbell):
        """Plot Campbell diagram by using Plotly.

        Parameters
        ----------
        out_campbell : list
            Output parameters of 'run_campbell'.

        Returns
        -------
        fig : Plotly graph_objects.Figure()
            The figure object with the plot.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> Campbell = rotor.run_campbell(10, 9000, 20, 100)
        >>> fig_campbell = PostProcessing().plot_campbell(Campbell).show()
        """
        omega = out_campbell[0]
        omegaHz = out_campbell[1]
        freqNAT = out_campbell[2]
        OMEGAunstable = out_campbell[3]
        Cs = out_campbell[4]
        speedI = out_campbell[5]
        speedF = out_campbell[6]
        maxFREQ = out_campbell[7]

        figure = go.Figure()

        figure.add_trace(
            go.Scatter(
                x=omega,
                y=omegaHz,
                mode="lines",
                line=dict(color="rgb(255, 0, 0)", width=1, dash="dot"),
            )
        )
        rows, cols = freqNAT.shape
        for ii in range(0, rows):
            figure.add_trace(
                go.Scatter(
                    x=omega,
                    y=freqNAT[ii, :],
                    mode="lines",
                    line=dict(color="rgb(0, 128, 255)", width=3, dash="dot"),
                )
            )

        if len(Cs) == 0:
            figure.add_trace(
                go.Scatter(
                    x=np.array([OMEGAunstable, OMEGAunstable]),
                    y=np.array([0, freqNAT], dtype=object),
                    mode="lines",
                    line=dict(color="rgb(0, 255, 0)", width=2, dash="dot"),
                )
            )

        figure.update_layout(
            title=dict(
                text=f"Campbell Diagram",
                font=dict(family="Times New Roman", size=16),
            )
        )
        figure.update_xaxes(
            title=dict(
                text=f"Rotation speed (rev/min)",
                font=dict(family="Times New Roman", size=16),
            ),
            tickfont=dict(family="Times New Roman", size=16),
        )

        figure.update_yaxes(
            title=dict(
                text=f"Frequency (Hz)", font=dict(family="Times New Roman", size=16)
            ),
            tickfont=dict(family="Times New Roman", size=16),
        )
        figure.update_layout(xaxis_range=[speedI, speedF])
        figure.update_layout(yaxis_range=[0.1, maxFREQ])
        figure.update_traces(showlegend=False, selector=dict(type="scatter"))

        return figure

    def plot_fulldfft(self, range_freq=None, **kwargs):
        """Plot response in frequency domain - full spectrum fourier transform vector by using Plotly.

        Parameters
        ----------
        range_freq : list, optional
            Units for the x axis.
            Default is "Hz"
        kwargs : optional
            Additional key word arguments can be passed to change the plot layout only
            (e.g. width=1000, height=800, yaxis_type="log", ...).
            *See Plotly Python Figure Reference for more information.

        Returns
        -------
        fig : Plotly graph_objects.Figure()
            The figure object with the plot.

        References
        ----------
        .. [1] 'Application of full spectrum to rotating machinery diagnostics' by Ph.D. Paul Goldman ...
        & Ph.D. Agnes Muszynska. Avaible at http://amconsulting.intellibit.com/499agnes.shtml.


        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> x = rotor.run(0, 0.01, 10, [1e-4], [0], 1200)
        >>> results = PostProcessing(rotor, [2], x).plot_fulldfft([-40, 40], yaxis_type="linear")[0].show()
        """
        window_type = "boxcar"
        results = self.output.yy
        temp = self.output.T
        self.DOF = self.output.DOF
        if self.DOF == 6:
            aux_dof = 2
        else:
            aux_dof = 1
        dt = temp[1] - temp[0]
        fig = []

        for i in list(range(len(self.probe))):

            dofx = self.probe[i] * self.DOF  # Number of degrees os freedom
            dofz = self.probe[i] * self.DOF + aux_dof

            rows, cols = results.shape

            signal_x = results[dofx, :]
            signal_z = results[dofz, :]

            window = signal.windows.get_window(window_type, len(signal_x))

            b = np.floor(len(signal_x) / 2)
            c = len(signal_x)
            df = 1 / (c * dt)

            x_amp = sp.fft(signal_x * window)[: int(b)]
            x_amp = x_amp * 2 / c
            self.x_phase = np.angle(x_amp)
            self.x_amp = np.abs(x_amp)

            b = np.floor(len(signal_z) / 2)
            c = len(signal_z)
            z_amp = sp.fft(signal_z * window)[: int(b)]
            z_amp = z_amp * 2 / c
            self.z_phase = np.angle(z_amp)
            self.z_amp = np.abs(z_amp)

            freq = np.arange(0, df * b, df)
            freq = freq[: int(b)]  # Frequency vector

            self.freq = freq

            freq, f_amp = self.forward(freq_cut=range_freq[1])
            b_amp = self.backward(freq_cut=range_freq[1])[1]
            full_freq = np.concatenate(
                (-freq[-1:0:-1], freq)
            )  # full spectrum frequency -> [-n..0..n]
            full_amp = np.concatenate(
                (b_amp[-1:0:-1], f_amp)
            )  # full spectrum amplitude -> [backward forward]

            figure = go.Figure()

            # if plot_type is None or plot_type == "linear":
            #     pass
            # elif plot_type == "log":
            #     figure.update_layout(yaxis_type="log")
            # elif plot_type == "dB":
            #     y = 20 * np.log10(y)
            #     figure.update_layout(
            #         title=f"Full Spectrum Fourier Transform - Probe {i + 1}",
            #         xaxis_title="Frequency [Hz]",
            #         yaxis_title="Amplitude [dB]",
            #     )

            figure.add_trace(go.Scatter(x=full_freq, y=full_amp, mode="lines"))
            figure.update_layout(
                title=dict(
                    text=f"Full Spectrum Fourier Transform - Probe {i + 1}",
                    font=dict(family="Times New Roman", size=16),
                )
            )

            figure.update_yaxes(
                title=dict(
                    text=f"Amplitude (m)", font=dict(family="Times New Roman", size=16)
                ),
                tickfont=dict(family="Times New Roman", size=16),
            )
            figure.update_xaxes(
                title=dict(
                    text=f"Frequency (Hz)", font=dict(family="Times New Roman", size=16)
                ),
                tickfont=dict(family="Times New Roman", size=16),
            )
            figure.update_layout(**kwargs)
            figure.update_layout(
                legend=dict(font=dict(family="Times New Roman", size=16))
            )
            figure.update_layout(xaxis_range=[range_freq[0], range_freq[1]])
            fig.append(figure)
        return fig

    def forward(self, freq_cut=float):
        """Calculates the forward spectrum fourier transform.

        Parameters
        ----------
        freq_cut : float, optional
            Choose the frequency cut of the plot.

        Returns
        -------
        freq : numpy.ndarray
            Frequency vector
        f_amp : numpy.ndarray
            Forward amplitude vector
        """
        f_amp = np.abs(
            (
                self.x_amp ** 2
                + self.z_amp ** 2
                - 2 * self.x_amp * self.z_amp * np.sin(self.x_phase - self.z_phase)
            )
            ** 0.5  # Forward amplitude
        )
        freq = self.freq
        if freq_cut is None:
            pass
        else:
            aux = freq_cut - self.freq
            where = np.argmin(np.abs(aux))
            f_amp = f_amp[: int(where)]
            freq = self.freq[: int(where)]

        return freq, f_amp

    def backward(self, freq_cut=float):
        """Calculates the backward spectrum fourier transform.

        Parameters
        ----------
        freq_cut : float, optional
            Choose the frequency cut of the plot.

        Returns
        -------
        freq : numpy.ndarray
            Frequency vector
        b_amp : numpy.ndarray
            Backward amplitude vector
        """
        b_amp = np.abs(
            (
                self.x_amp ** 2
                + self.z_amp ** 2
                + 2 * self.x_amp * self.z_amp * np.sin(self.x_phase - self.z_phase)
            )
            ** 0.5  # Backward amplitude
        )
        freq = self.freq
        if freq_cut is None:
            pass
        else:
            aux = freq_cut - self.freq
            where = np.argmin(np.abs(aux))
            b_amp = b_amp[: int(np.round(where))]
            freq = self.freq[: int(np.round(where))]

        return freq, b_amp