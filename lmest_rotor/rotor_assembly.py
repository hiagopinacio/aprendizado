import numpy as np
import plotly.graph_objects as go
import scipy.linalg
from scipy.sparse import linalg as las

from lmest_rotor import Material

from .bearing_element import BearingElement4DoF, BearingElement6DoF
from .disk_element import DiskElement4DoF, DiskElement6DoF
from .numerical_integration import NewmarkMethod
from .shaft_element import ShaftElement4DoF, ShaftElement6DoF


class Rotor(object):
    """A rotor object.

    This class will create a rotor with the shaft, disk and bearing elements provided.

    Parameters
    ----------
    shaft_elements : list
        List with the shaft elements.
    disk_elements : list
        List with the disk elements.
    bearing_elements : list
        List with the bearing elements.

    Returns
    -------
    A rotor object.

    Attributes
    ----------
    MM : array
        Global mass matrix.
    KK : array
        Global stiffness matrix.
    CCgyros: array
        Global gyroscopic matrix.
    CCtotal: array
        Global damping matrix

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> rotor = lm.rotor_example()
    >>> rotor.MM
    array(30x30)
    """

    def __init__(
        self,
        shaft_elements,
        disk_elements=None,
        bearing_elements=None,
    ):

        self.shaft_elements = shaft_elements
        self.disk_elements = disk_elements
        self.bearing_elements = bearing_elements
        self.DOF = self.shaft_elements[0].DOF

        self.Nele = len(self.shaft_elements)
        self.ndof = (self.Nele + 1) * self.DOF
        self.MM = np.zeros((self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1)))
        self.KK = np.zeros((self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1)))
        self.KKest = np.zeros((self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1)))
        self.KKst = np.zeros((self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1)))
        self.KKf = np.zeros((self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1)))
        self.KKt = np.zeros((self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1)))
        self.CCgyros = np.zeros(
            (self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1))
        )
        self.CCbearing = np.zeros(
            (self.DOF * (self.Nele + 1), self.DOF * (self.Nele + 1))
        )

        self._assemblyGlobal()

        if disk_elements is None:
            self.disk_elements = []
        if bearing_elements is None:
            self.bearing_elements = []

    def _conectmatrix(self):
        """Assembling the connectivity matrix.

        Returns
        -------
        Connectivity matrix.
        """
        IncN = np.zeros((self.Nele, 2))
        for i in range(self.Nele):
            IncN[i, 0] = i
            IncN[i, 1] = i + 1

        IncG = np.zeros((self.Nele, self.DOF * 2))
        for e in range(self.Nele):
            if self.DOF == 6:
                IncGRef = [
                    self.DOF * IncN[e, 0] + 0,
                    self.DOF * IncN[e, 0] + 1,
                    self.DOF * IncN[e, 0] + 2,
                    self.DOF * IncN[e, 0] + 3,
                    self.DOF * IncN[e, 0] + 4,
                    self.DOF * IncN[e, 0] + 5,
                    self.DOF * IncN[e, 1] + 0,
                    self.DOF * IncN[e, 1] + 1,
                    self.DOF * IncN[e, 1] + 2,
                    self.DOF * IncN[e, 1] + 3,
                    self.DOF * IncN[e, 1] + 4,
                    self.DOF * IncN[e, 1] + 5,
                ]
            else:
                IncGRef = [
                    self.DOF * IncN[e, 0] + 0,
                    self.DOF * IncN[e, 0] + 1,
                    self.DOF * IncN[e, 0] + 2,
                    self.DOF * IncN[e, 0] + 3,
                    self.DOF * IncN[e, 1] + 0,
                    self.DOF * IncN[e, 1] + 1,
                    self.DOF * IncN[e, 1] + 2,
                    self.DOF * IncN[e, 1] + 3,
                ]

            Ref = np.arange(self.DOF * 2)
            IncG[e, Ref] = IncG[e, Ref] + IncGRef
        return IncG

    def runModal(self, numberMODES=None, Faxial=None):
        """Run modal analysis.

        Method to calculate eigenvalues and eigvectors for a given rotor system.

        Parameters
        ----------
        numberMODES : int, optional
            Number of modes used to reduce the model.
        Faxial : float, optional
            Axial force.

        Returns
        -------
        naturalFREQ : array
            Rotor's natural frequencies in rad/s.
        MODALmatrix : array
            Rotor modal matrix.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> rotor.runModal()
        (array(30,), array(30x12))
        """

        if numberMODES == None:
            numberMODES = 12

        if Faxial == None:
            Faxial = 0

        self.numberMODES = numberMODES
        self.Faxial = Faxial
        naturalFREQ, ModMat = scipy.linalg.eigh(
            (self.KKest + self.Faxial * self.KKf),
            self.MM,
            type=1,
            turbo=False,
        )
        naturalFREQ = np.sort(np.sqrt(abs(naturalFREQ)) / (2 * np.pi))
        MODALmatrix = ModMat[:, : self.numberMODES]

        return naturalFREQ, MODALmatrix

    def _montPseudoModal(self, numberMODES, Faxial):
        """Pseudo-modal method.

        This method uses the modal base to assemble the modal matrices (matrices with order reduction)
        to be used in the numerical integration. The pseudo-modal method allows an effective reduction
        in computational time.

        Parameters
        ----------
        numberMODES : int, optional
            Number of modes used to reduce the model
        Faxial : float, optional
            Axial force

        Attributes
        ----------
        MMmodal : array
            Modal mass  matrix.
        KKmodal : array
            Modal stiffness matrix.
        CCgyrosmodal: array
            Modal gyroscopic matrix.
        CCtotalmodal: array
            Modal damping matrix.

        References
        ----------
        .. [1] Lalanne and G. Ferraris, (1998) Rotordynamics prediction in engineering. Wiley.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> rotor._montPseudoModal(12,0)
        None
        >>> rotor.MMmodal
        array(12,12)
        """
        self.MODALmatrix = self.runModal(numberMODES, Faxial)[1]
        modmat_t = self.MODALmatrix.T
        self.MMmodal = ((modmat_t).dot(self.MM)).dot(self.MODALmatrix)
        self.KKmodal = ((modmat_t).dot(self.KK)).dot(self.MODALmatrix)
        self.KKestmodal = ((modmat_t).dot(self.KKest)).dot(self.MODALmatrix)
        self.KKstmodal = ((modmat_t).dot(self.KKst)).dot(self.MODALmatrix)
        self.KKfmodal = ((modmat_t).dot(self.Faxial * self.KKf)).dot(self.MODALmatrix)
        self.KKtmodal = ((modmat_t).dot(self.KKt)).dot(self.MODALmatrix)
        self.CCgyrosmodal = ((modmat_t).dot(self.CCgyros)).dot(self.MODALmatrix)
        self.CCbearingmodal = ((modmat_t).dot(self.CCbearing)).dot(self.MODALmatrix)

        GRA = np.zeros((self.ndof))
        ag = 1
        if self.DOF == 6:
            for ii in range(2, len(GRA) + 1, self.DOF):
                GRA[ii] = -9.81 * ag
        else:
            for ii in range(1, len(GRA) + 1, self.DOF):
                GRA[ii] = -9.81 * ag

        self.W = (self.MM).dot(GRA)

        self.Wmodal = (modmat_t).dot(self.W)
        self.CCtotalmodal = ((modmat_t).dot(self.CCtotal)).dot(self.MODALmatrix)

        FFaxial = np.zeros((self.ndof))
        if self.DOF == 6:
            aux = 5
        if self.DOF == 4:
            aux = 3
        FFaxial[self.ndof - aux] = Faxial
        self.FFaxialmodal = (modmat_t).dot(FFaxial)

    def _assemblyGlobal(self):
        """Assembling the Global matrices."""
        if self.disk_elements is not None:
            n_disks = len(self.disk_elements)
        else:
            n_disks = 0

        if self.bearing_elements is not None:
            n_bearings = len(self.bearing_elements)
        else:
            n_bearings = 0

        IncG = self._conectmatrix()

        for e in range(self.Nele):
            Inc = IncG[e, :]
            M_shaft = self.shaft_elements[e].M()
            K_shaft = self.shaft_elements[e].K()
            Kest_shaft = self.shaft_elements[e].K()
            Kst_shaft = self.shaft_elements[e].Kst()
            Kf_shaft = self.shaft_elements[e].Kf()
            Kt_shaft = self.shaft_elements[e].Kt()
            G_shaft = self.shaft_elements[e].G()
            Cbearing = self.shaft_elements[e].G() * 0

            for ii in range(n_disks):
                if (e == self.disk_elements[ii].n) or (
                    self.disk_elements[ii].n == self.Nele
                    and e + 1 == self.disk_elements[ii].n
                ):

                    if self.disk_elements[ii].n < self.Nele:
                        aux_n = 1
                    else:
                        aux_n = 2

                    M_disk = self.disk_elements[ii].M()
                    M_shaft[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += M_disk

                    K_disk = self.disk_elements[ii].Kst()
                    K_shaft[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += K_disk

                    Kst_disk = self.disk_elements[ii].Kst()
                    Kst_shaft[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += Kst_disk

                    G_disk = self.disk_elements[ii].G()
                    G_shaft[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += G_disk

            for ii in range(n_bearings):
                if (e == self.bearing_elements[ii].n) or (
                    self.bearing_elements[ii].n == self.Nele
                    and e + 1 == self.bearing_elements[ii].n
                ):

                    if self.bearing_elements[ii].n < self.Nele:
                        aux_n = 1
                    else:
                        aux_n = 2

                    K_bearing = self.bearing_elements[ii].K()
                    K_shaft[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += K_bearing

                    Kest_bearing = self.bearing_elements[ii].KbearingEst()
                    Kest_shaft[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += Kest_bearing

                    C_bearing = self.bearing_elements[ii].C()
                    Cbearing[
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                        self.DOF * aux_n - self.DOF : self.DOF * aux_n,
                    ] += C_bearing

            self.MM[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += M_shaft

            self.KK[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += K_shaft

            self.KKest[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += Kest_shaft

            self.KKst[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += Kst_shaft

            self.KKf[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += Kf_shaft

            self.KKt[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += Kt_shaft

            self.CCgyros[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += G_shaft

            self.CCbearing[
                int(Inc[0]) : int(Inc[-1] + 1), int(Inc[0]) : int(Inc[-1] + 1)
            ] += Cbearing

        self.CCtotal = (self.shaft_elements[0].alpha) * (self.MM) + (
            self.shaft_elements[0].beta
        ) * (self.KK)

    def run(
        self,
        T_inicial,
        dt,
        T_final,
        massunb,
        phaseunb,
        speedI,
        speedF=None,
        Faxial=None,
        TorqueI=None,
        TorqueF=None,
        numberMODES=None,
        print_progress=None,
    ):
        """Unbalanced response for a mdof system.

        This method returns the unbalanced response for a mdof system
        given some parameters of time, magnitude and, phase of the unbalance,
        operation speed and, eventually external forces.

        Parameters
        ----------
        T_inicial : float
            Inicial time.
        dt : float
            Time step.
        T_final : float
            Final time.
        massunb : list
            List with the unbalance magnitude [kg.m].
        phaseunb : list
            List with the unbalance phase [rad].
        speedI : float
            Initial rotating speed [RPM].
        speedF : float, optional
            Final rotating speed [RPM].
            Default is equal to speedI.
        Faxial : float, optional
            Axial force that to be inserted in the first node [N].
            Default is 0.
        TorqueI : float, optional
            Initial torque that to be inserted in the first node [N.m].
        TorqueF : float, optional
            Final torque that to be inserted in the first node [N.m].
            Default is 0 or equal to TorqueI.
        numberMODES : int, optional
            Number of modes used to reduce the model.
            Default is 12.
        print_progress : bool
        Set it True, to print the time iterations and the total time spent.
        False by default.


        Returns
        -------
        out_t : object
            Time response of the system.

        Attributes
        ----------
        yy : array
            Amplitude of time response for all degrees of freedom.
        T : array
            Time vector response.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> x = rotor.run(0, 0.01, 10, [5e-4], [-np.pi / 2], 1200)
        >>> x.yy
        array(30x1001)
        """
        self.T_inicial = T_inicial
        self.dt = dt
        self.T_final = T_final
        self.MassUnb = massunb
        self.PhaseUnb = phaseunb
        self.speedI = speedI

        if print_progress == None:
            self.print_progress = False
        else:
            self.print_progress = print_progress

        if speedF == None:
            self.speedF = speedI
        else:
            self.speedF = speedF

        if Faxial == None:
            self.Faxial = 0
        else:
            self.Faxial = Faxial

        if TorqueI == None and TorqueF == None:
            self.TorqueI = 0
            self.TorqueF = 0
        elif (TorqueI is not None) and (TorqueF == None):
            self.TorqueI = TorqueI
            self.TorqueF = TorqueI
        elif (TorqueI is not None) and (TorqueF is not None):
            self.TorqueI = TorqueI
            self.TorqueF = TorqueF

        if numberMODES == None:
            numberMODES = 12

        if len(self.MassUnb) != len(self.PhaseUnb):
            raise Exception(
                "The unbalance magnitude vector and phase must have the same size!"
            )
        self._montPseudoModal(numberMODES, self.Faxial)

        self.n_disk = len(self.disk_elements)
        if self.n_disk != len(self.MassUnb):
            raise Exception("The number of discs and unbalances must agree!")
        self.ndofd = np.zeros(len(self.disk_elements))

        for ii in list(range(self.n_disk)):
            self.ndofd[ii] = (self.disk_elements[ii].n) * self.DOF

        T = np.arange(self.T_inicial, self.T_final + self.dt, self.dt)

        # parameters for the time integration
        self.lambdat = 0.00001

        warI = self.speedI * np.pi / 30
        warF = self.speedF * np.pi / 30

        # pre-processing of auxilary variuables for the time integration
        self.sA = (
            warI * np.exp(-self.lambdat * self.T_final)
            - warF * np.exp(-self.lambdat * self.T_inicial)
        ) / (
            np.exp(-self.lambdat * self.T_final)
            - np.exp(-self.lambdat * self.T_inicial)
        )
        self.sB = (warF - warI) / (
            np.exp(-self.lambdat * self.T_final)
            - np.exp(-self.lambdat * self.T_inicial)
        )

        self.sAT = (
            self.TorqueI * np.exp(-self.lambdat * self.T_final)
            - self.TorqueF * np.exp(-self.lambdat * self.T_inicial)
        ) / (
            np.exp(-self.lambdat * self.T_final)
            - np.exp(-self.lambdat * self.T_inicial)
        )
        self.sBT = (self.TorqueF - self.TorqueI) / (
            np.exp(-self.lambdat * self.T_final)
            - np.exp(-self.lambdat * self.T_inicial)
        )

        self.angular_position = (
            self.sA * T
            - (self.sB / self.lambdat) * np.exp(-self.lambdat * T)
            + (self.sB / self.lambdat)
        )

        # Omega = self.speedI * np.pi / 30
        self.Omega = self.sA + self.sB * np.exp(-self.lambdat * T)
        self.AccelV = -self.lambdat * self.sB * np.exp(-self.lambdat * T)
        self.TorqueV = self.sAT + self.sBT * np.exp(-self.lambdat * T)
        self.tetaUNB = np.zeros((len(self.PhaseUnb), len(self.angular_position)))

        unbx = np.zeros(len(self.angular_position))
        unby = np.zeros(len(self.angular_position))

        FFunb = np.zeros((self.ndof, len(T)))

        for ii in list(range(self.n_disk)):
            self.tetaUNB[ii, :] = self.angular_position + self.PhaseUnb[ii] + np.pi / 2

            unbx = self.MassUnb[ii] * (self.AccelV) * (
                np.cos(self.tetaUNB[ii, :])
            ) - self.MassUnb[ii] * ((self.Omega ** 2)) * (np.sin(self.tetaUNB[ii, :]))

            unby = -self.MassUnb[ii] * (self.AccelV) * (
                np.sin(self.tetaUNB[ii, :])
            ) - self.MassUnb[ii] * (self.Omega ** 2) * (np.cos(self.tetaUNB[ii, :]))

            if self.DOF == 6:
                FFunb[int(self.ndofd[ii]), :] += unbx
                FFunb[int(self.ndofd[ii] + 2), :] += unby
            else:
                FFunb[int(self.ndofd[ii]), :] += unbx
                FFunb[int(self.ndofd[ii] + 1), :] += unby

        self.Funbmodal = (self.MODALmatrix.T).dot(FFunb)

        FFtorque = np.zeros((self.ndof, len(T)))
        FFtorque[4, :] = self.TorqueV
        self.FFtorquemodal = (self.MODALmatrix.T).dot(FFtorque)

        out_t = NewmarkMethod(
            self.DOF,
            self.ndof,
            self.dt,
            T,
            self.Omega,
            self.AccelV,
            self.TorqueV,
            self.Funbmodal,
            self.MODALmatrix,
            self.MMmodal,
            self.KKmodal,
            self.KKstmodal,
            self.KKfmodal,
            self.KKtmodal,
            self.CCgyrosmodal,
            self.CCtotalmodal,
            self.Wmodal,
            self.FFaxialmodal,
            self.FFtorquemodal,
            self.print_progress,
        )

        return out_t

    def run_campbell(
        self,
        speedI,
        speedF,
        speedSTEP,
        maxFREQ,
        numberMODES=None,
        TorqueI=None,
        TorqueF=None,
        TorqueSTEP=None,
        Faxial=None,
    ):
        """Calculate the Campbell diagram.

        Construction of the Campbell diagram with the determination of the
        instability threshold.

        Parameters
        ----------
        speedI : float
            Initial rotating speed [RPM].
        speedF : float, optional
            Final rotating speed [RPM].
        speedSTEP : float
            Increment of speed [RPM].
        maxFREQ : float
            Maximum frequency of the diagram [RPM].
        numberMODES : int, optional
            Number of modes used to reduce the model.
            Default is 12.
        TorqueI : float, optional
            Initial torque that to be inserted in the first node [N.m].
            Default is 0.
        TorqueF : float, optional
            Final torque that to be inserted in the first node [N.m].
            Default is 0 or equal to TorqueI.
        TorqueSTEP : float, optional
            Increment of the torque [N.m].
        Faxial : float, optional
            Axial force that to be inserted in the first node [N].
            Default is 0.

        Returns
        -------
        omega : array
            Array containing the rotation speed in RPM.
        omegaHz : array
            Array containing the rotation speed in Hz.
        freqNAT : array
            Rotor's natural frequencies in rad/s.
        OMEGAunstable : float
            Instability threshold.
        Cs_new : array
            Index of positive instability threshold.
        speedI : float
            Initial rotating speed [RPM].
        speedF : float
            Final rotating speed [RPM].
        maxFREQ : float
            Maximum frequency of the diagram [RPM].

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> rotor = lm.rotor_example()
        >>> Campbell = rotor.run_campbell(10, 9000, 20, 100)
        >>> fig_campbell = PostProcessing().plot_campbell(Campbell).show()
        """

        if Faxial == None:
            self.Faxial = 0
        else:
            self.Faxial = Faxial

        if TorqueI == None and TorqueF == None and TorqueSTEP == None:
            self.TorqueI = 0
            self.TorqueF = 0
            self.TorqueSTEP = 0
            TorqueV = np.array([self.TorqueI])
        elif (TorqueI is not None) and (TorqueF == None) and TorqueSTEP == None:
            self.TorqueI = TorqueI
            self.TorqueF = TorqueI
            self.TorqueSTEP = 0
            TorqueV = np.array([self.TorqueI])
        elif (TorqueI is not None) and (TorqueF is not None) and TorqueSTEP == None:
            self.TorqueI = TorqueI
            self.TorqueF = TorqueF
            self.TorqueSTEP = 1
            TorqueV = np.arange(
                self.TorqueI, self.TorqueF + self.TorqueSTEP, self.TorqueSTEP
            )
        elif (TorqueI is not None) and (TorqueF is not None) and TorqueSTEP is not None:
            self.TorqueI = TorqueI
            self.TorqueF = TorqueF
            self.TorqueSTEP = TorqueSTEP
            TorqueV = np.arange(
                self.TorqueI, self.TorqueF + self.TorqueSTEP, self.TorqueSTEP
            )

        TorqueM = sum(TorqueV) / float(len(TorqueV))

        if numberMODES == None:
            numberMODES = 12

        self._montPseudoModal(numberMODES, self.Faxial)

        # Solution of the eigenvalue problem
        aux = 0
        speed_r = np.arange(speedI, speedF + speedSTEP, speedSTEP)
        aux_K = self.KKmodal + TorqueM * self.KKtmodal + self.KKfmodal
        # aux_inv = np.linalg.solve(self.MMmodal, aux_K)
        aux_inv = -np.linalg.inv(self.MMmodal).dot(aux_K)
        aux_invM = -np.linalg.inv(self.MMmodal)
        stabUNSTAB = np.zeros((len(self.MMmodal), len(speed_r)))
        freqNAT = np.zeros((len(self.MMmodal), len(speed_r)))

        coef1 = np.concatenate(
            (
                np.zeros((len(self.MMmodal), len(self.MMmodal))),
                np.eye(len(self.MMmodal)),
            ),
            axis=1,
        )

        for omega in speed_r:

            OMEGArads = omega * np.pi / 30

            coef2 = np.concatenate(
                (
                    aux_inv,
                    aux_invM.dot((self.CCtotalmodal + OMEGArads * self.CCgyrosmodal)),
                ),
                axis=1,
            )
            AA = np.concatenate((coef1, coef2), axis=0)

            d, _ = np.linalg.eig(AA)
            # d, _ = las.eigs(AA, k=24, which="LM")

            w = np.sort(np.imag(d[np.arange(0, len(d), 2)]))
            w = w[0 : len(self.MMmodal)]
            freqNAT[:, aux] = w / (2 * np.pi)

            r = np.sort(np.real(d[np.arange(0, len(d), 2)]))
            r = r[0 : len(self.MMmodal)]
            stabUNSTAB[:, aux] = r

            aux += 1

        # L = np.where((freqNAT[:, 0] > 0) & (freqNAT[:, 0] < maxFREQ))[0]
        # freqNAT = freqNAT[L, :]

        omega = speed_r
        omegaHz = omega / 60

        Cs = np.where(stabUNSTAB > 0)[1]
        Cs_new = Cs

        if len(Cs) == 0:
            Cs = 1

        OMEGAunstable = omega[(np.array([Cs])[0])]
        # OMEGAunstable = omega[Cs[0]]
        return omega, omegaHz, freqNAT, OMEGAunstable, Cs_new, speedI, speedF, maxFREQ


def rotor_example():
    """Create a rotor as example.

    This function returns an instance of a simple rotor with
    two shaft elements, one disk and two simple bearings.
    The purpose of this is to make available a simple model
    so that doctest can be written using this.

    Returns
    -------
    An instance of a rotor object.

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> rotor = lm.rotor_example()
    >>> rotor.MM
    array(30,30)
    """
    #  Rotor with 4 shaft elements 1 disk and 2 bearings
    steel = Material.from_data("steel")
    L = np.array([0, 25, 50, 75, 100]) / 1000
    i_d = np.array([0, 0, 0, 0])
    o_d = np.array([0.019, 0.019, 0.019, 0.019])
    L_size = [L[i] - L[i - 1] for i in range(1, len(L))]
    L_total = np.concatenate(([L_size], [i_d], [o_d]), axis=0)
    shaft_elem = [
        ShaftElement6DoF(
            material=steel,
            L=L_total[0, l],
            id=L_total[1, l],
            od=L_total[2, l],
            alpha=8.0501,
            beta=1.0e-5,
        )
        for l in range(len(L_size))
    ]
    disk = DiskElement6DoF(n=3, m=(300e-3), Id=0.001, Ip=0.002)
    bearing1 = BearingElement6DoF(n=0, kxx=1e5, cxx=20, kzz=1e6, czz=40)
    bearing2 = BearingElement6DoF(n=4, kxx=1e5, cxx=20, kzz=1e6, czz=40)

    return Rotor(shaft_elem, [disk], [bearing1, bearing2])
