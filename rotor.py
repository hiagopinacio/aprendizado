import numpy as np
import lmest_rotor as lm

def mount_rotor():

    steel2 = lm.Material(name="Steel", rho=7850, E=2.17e11, nu=0.2992610837438423) # Sets the shaft material
    # fmt: off
    # Position of the nodes on the FE model (ie, distance from node #01)
    L = np.array(
            [0  ,  25,  64, 104, 124, 143, 175, 207, 239, 271,
            303, 335, 345, 355, 380, 408, 436, 466, 496, 526,
            556, 586, 614, 647, 657, 667, 702, 737, 772, 807,
            842, 862, 881, 914]
            )/ 1000
    # fmt: on
    i_d = np.zeros(len(L) - 1) # Inner radius of the shaft
    o_d = np.ones(len(L) - 1) * 0.019 # Outer radius of the shaft
    L_size = [L[i] - L[i - 1] for i in range(1, len(L))] # Sets a list with the size of each shaft element

    # L_total = np.concatenate(([L_size], [i_d], [o_d]), axis=0)
    L_total = np.vstack(([L_size], [i_d], [o_d])) #Stack arrays in sequence vertically (row wise)

    # Sets a list with the shaft elements (objects)
    shaft_elem = [                            
        lm.ShaftElement6DoF(
            material=steel2,
            L=L_total[0, l],
            id=L_total[1, l],
            od=L_total[2, l],
            alpha=8.0501,
            beta=1.0e-5,
        )
        for l in range(len(L_size))
    ]

    Id = 0.003844540885417
    Ip = 0.007513248437500

    disk0 = lm.DiskElement6DoF(n=12, m=2.6375, Id=Id, Ip=Ip) # Sets an object with the disk element  
    disk1 = lm.DiskElement6DoF(n=24, m=2.6375, Id=Id, Ip=Ip) # Sets an object with the disk element  

    kxx1 = 4.40e5
    kyy1 = 0
    kzz1 = 4.6114e5
    cxx1 = 27.4
    cyy1 = 0
    czz1 = 2.505
    kxx2 = 9.50e5
    kyy2 = 0
    kzz2 = 1.09e8
    cxx2 = 50.4
    cyy2 = 0
    czz2 = 100.4553

    # Sets an object with the bearing element  
    bearing0 = lm.BearingElement6DoF(
        n=4, kxx=kxx1, kyy=kyy1, cxx=cxx1, cyy=cyy1, kzz=kzz1, czz=czz1
    )

    # Sets an object with the bearing element  
    bearing1 = lm.BearingElement6DoF(
        n=31, kxx=kxx2, kyy=kyy2, cxx=cxx2, cyy=cyy2, kzz=kzz2, czz=czz2
    )

    # Sets a rotor object (instantiates the rotor class with a list of the shaft 
    # elements, a list of the disk elements, and a list of the bearing elements)
    rotor = lm.Rotor(shaft_elem, [disk0, disk1], [bearing0, bearing1])

    return rotor